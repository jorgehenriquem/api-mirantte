<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TiposImovel extends Model
{
    protected $guarded = ['id'];
    protected $table = "categorias_tipo_imovel";
}
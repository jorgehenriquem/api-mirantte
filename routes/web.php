<?php

use App\Models\Imoveis;
use App\Http\Controllers\ImoveisController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('/api/imoveis/{id}', 'ImoveisController@getImoveis');

/**
 * @api {get} /api/imoveis/{id} Busca de Imoveis
 * @apiName MirantteAPI
 * @apiDescription Busca Imovel pelo ID
 * @apiGroup Imoveis
 * @apiParam (Parameter) {number} [id]  Id do imovel e também Codigo do Imovel.
 * @apiParam (Parameter) {String} [adress_zipcode]  CEP do Imovel .
 * @apiParam (Parameter) {String} [adress]  Logradouro do Imovel.
 * @apiParam (Parameter) {String} [adress_number]  Numero do Imovel.
 * @apiParam (Parameter) {String} [adress_complement]  Complemento do Imovel.
 * @apiParam (Parameter) {String} [adress_neighborhood] Bairro do Imovel.
 * @apiParam (Parameter) {String} [adress_city]  Referente ao cidade do imovel.
 * @apiParam (Parameter) {String} [adress_uf]  Estado do Imovel.
 * @apiParam (Parameter) {float} [adress_map_lat]  Latitude do Imovel.
 * @apiParam (Parameter) {float} [adress_map_long]  Longitude do Imovel.
 * @apiParam (Parameter) {tinyInt} [bedrooms]  Quantidades de quartos que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [suites]   Quantidades de suites que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [wc]  Quantidades de banheiros que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [rooms]   Quantidades de salas que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [parking_spaces]   Quantidades de vagas de garagem que o Imovel possui.
 * @apiParam (Parameter) {float} [useful_area] Area Util do Imovel.
 * @apiParam (Parameter) {float} [common_area] Area Comun do Imovel.
 * @apiParam (Parameter) {float} [building_area] Area Construida do Imovel.
 * @apiParam (Parameter) {float} [total_area] Area Total do Imovel.
 * @apiParam (Parameter) {float} [terrain_area] Area do terreno.
 * @apiParam (Parameter) {float} [metragem_terreno]  Numero do Imovel.
 * @apiParam (Parameter) {decimal} [amount_sale]  Valor de venda do imovel.
 * @apiParam (Parameter) {decimal} [amount_location]  Valor de Aluguel do imovel.
 * @apiParam (Parameter) {decimal} [amount_condominium]  Valor do condominio do imovel.
 * @apiParam (Parameter) {decimal} [amount_iptu]  Valor do IPTU do imovel.
 * @apiParam (Parameter) {tinyInt} [permutation_accept]  Se o imovel aceita permulta.
 * @apiParam (Parameter) {text} [description]  Descricao completa do imovel.
 * @apiParam (Parameter) {integer} [agency_id]  Id referente a agencia onde esse imovel foi publicado.
 * @apiParam (Parameter) {String} [created_at]  Registro referente a quando o imovel foi criado.
 * @apiParam (Parameter) {String} [updated_at]  Registro referente a quando o imovel foi editado.
 * @apiParam (Photos) {number} [id]  Id da foto.
 * @apiParam (Photos) {String} [archive]  Nome do arquivo da imagem.
 * @apiParam (Photos) {tinyInt} [index]  Indice da foto.
 * @apiParam (Photos) {String} [description]  Descrição da foto.
 * @apiParam (Photos) {integer} [propertie_id]  Numero referente ao id do imovel pertencente a foto.


 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
 *       data: {
        "id": 75023,
        "adress_zipcode": "02765040",
        "adress": "VITO OSVALDO SAPONARA",
        "adress_number": "90",
        "adress_complement": " comple",
        "adress_neighborhood": "Jardim Cachoeira",
        "adress_city": "São Paulo",
        "adress_uf": "SP",
        "adress_map_lat": null,
        "adress_map_long": null,
        "bedrooms": 3,
        "suites": 1,
        "wc": 1,
        "rooms": 1,
        "parking_spaces": 2,
        "useful_area": 160,
        "common_area": null,
        "building_area": 160,
        "total_area": null,
        "terrain_area": 160,
        "metragem_terreno": " ",
        "amount_sale": "300000.00",
        "amount_location": null,
        "amount_condominium": null,
        "amount_iptu": null,
        "permutation_accept": 0,
        "description": "ÓTIMO SOBRADO! SÃO 3 DORMITÓRIOS SENDO 1 SUÍTE, SALA, COZINHA, QUINTAL, 2 VAGAS.",
        "agency_id": 1,
        "created_at": "2010-02-10 00:00:00",
        "updated_at": "2012-04-04 18:24:40",
        "adress_city_id": 4850,
 *     }
              {
*               photos: [
        {
                "id": 27,
                "archive": "001_01411_00005_29283_2010012216494670.jpg",
                "index": 1,
                "description": "VITO OSVALDO SAPONARA",
                "propertie_id": 75023,
        },
        {
                "id": 45,
                "archive": "001_00000_00000_70980015_2010020409392290.jpg",
                "index": 2,
                "description": "VARANDA",
                "propertie_id": 75023,
        }
                        ]
    }
 */



$router->get('/api/cidades', 'ImoveisController@getCidades');

/**
 * @api {get} /api/cidades Lista de cidades
 * @apiGroup Cidades
 * @apiDescription Traz todas as cidades
 * @apiParam (Parameter) {number} [id]  Id da Cidade.
 * @apiParam (Parameter) {integer} [state_id] ID do Estado .
 * @apiParam (Parameter) {integer} [county_code]  Codigo do municipio.
 * @apiParam (Parameter) {string} [name]  Nome da cidade.
 * @apiParam (Parameter) {string} [uf]  UF.

@apiSuccessExample Success:
 *     HTTP/1.1 200 OK
{
    "cities": [
        {
            "id": 4850,
            "state_id": 25,
            "county_code": 3550308,
            "name": "São Paulo",
            "uf": "SP"
        },
        {
            "id": 4204,
            "state_id": 19,
            "county_code": 3304557,
            "name": "Rio de Janeiro",
            "uf": "RJ"
        },
            ]

}
 */

$router->get('/api', 'ImoveisController@index');





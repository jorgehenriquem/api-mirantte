<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Imoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address', 45);
            $table->string('address_number', 10);
            $table->string('address_complement', 10);
            $table->string('address_zipcode', 10);
            $table->string('address_neighborhood', 80); 
            $table->string('address_city', 80); 
            $table->string('address_uf', 2); 
            $table->float('address_map_lat'); 
            $table->float('address_map_long');
            $table->decimal('amount_sale', 11, 2); 
            $table->decimal('amount_location', 10, 2); 
            $table->decimal('amount_condominium', 10, 2); 
            $table->decimal('amount_iptu', 11, 2);
            $table->tinyInteger('wc')->unsigned();
            $table->tinyInteger('parking_spaces')->unsigned();
            $table->tinyInteger('rooms')->unsigned();
            $table->tinyInteger('bedrooms')->unsigned();
            $table->tinyInteger('suites')->unsigned();
            $table->float('useful_area')->unsigned();
            $table->float('common_area')->unsigned();
            $table->float('building_area')->unsigned();
            $table->float('total_area')->unsigned();
            $table->float('terrain_area')->unsigned();
            $table->float('terrain_footage')->unsigned();
            $table->tinyInteger('permutation_accept')->unsigned();
            $table->integer('agency_id', 10)->unsigned(); 
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
